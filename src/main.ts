import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import axios from "axios";
import VueMaterial from "vue-material";
import "vue-material/dist/vue-material.min.css";
import "vue-material/dist/theme/black-green-light.css";
import LodashForVue from "lodash-for-vue";

Vue.use(LodashForVue);
Vue.use(VueMaterial);
Vue.config.productionTip = false;
Vue.prototype.$http = axios;
axios.defaults.baseURL = "http://develop.api.erlaservers.eu/v1";
axios.defaults.headers = {
  Authorization: "Bearer " + localStorage.accessToken
};

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
