import axios from "axios";
class UserService {
  getUser() {
    return axios
      .get("/users/" + localStorage.userId)
      .then(response => {
        return response.data.user;
      })
      .catch(err => {
        console.log(err);
        return false;
      });
  }
  updateUser(data) {
    return axios
      .put("/users/" + localStorage.userId, data)
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log(err);
      });
  }
}
export default new UserService();
