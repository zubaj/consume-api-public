import axios from "axios";
class AuthenticationService {
  private baseUrl = "http://develop.api.erlaservers.eu/v1";
  authenticate(userName: string, password: string) {
    return axios
      .get(this.baseUrl + "/tokens", {
        headers: {
          Authorization: "Basic " + btoa(userName + ":" + password)
        }
      })
      .then(response => {
        localStorage.accessToken = response.data.token;
        localStorage.tokenExp = response.data.valid_until;
        localStorage.userId = response.data.payload.user.id;
        axios.defaults.headers = {
          Authorization: "Bearer " + response.data.token
        };
        return true;
      })
      .catch(err => {
        console.log(err);
        return false;
      });
  }
}
export default new AuthenticationService();
