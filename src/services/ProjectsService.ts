import axios from "axios";
class ProjectsService {
  getProjects() {
    return axios
      .get("/projects")
      .then(response => {
        return response;
      })
      .catch(err => {
        console.log(err);
        return false;
      });
  }

  createProject(data: object) {
    return axios
      .post("/projects", data)
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log(err);
      });
  }

  deleteProject(id) {
    return axios
      .delete("/projects/" + id)
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log(err);
      });
  }

  getProjectByID(id) {
    return axios
      .get("/projects/" + id)
      .then(res => {
        return res.data.project;
      })
      .catch(err => {
        console.log(err);
      });
  }

  updateProject(id, data) {
    return axios
      .put("/projects/" + id, data)
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log(err);
      });
  }

  searchProject(term) {
    return axios
      .get("/projects/search?q=" + term)
      .then(res => {
        return res.data.projects;
      })
      .catch(err => {
        console.log(err);
      });
  }
}

export default new ProjectsService();
