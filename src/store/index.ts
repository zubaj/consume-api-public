import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isAuthenticated: false,
    isError: false
  },
  mutations: {
    checkAuth(state, value) {
      state.isAuthenticated = value;
    },
    setError(state, value) {
      state.isError = value;
    }
  },
  actions: {},
  modules: {}
});
